# Eclectic Swingfolk website

This repo contains bespoke content for the (retired) Eclectic Swingfolk website. The original site was self hosted with WordPress.

The site's original domain has expired, and the content is preserved on GitHub for prosperity using Jekyll (rather than WordPress).

You can view the Jekyll version at [https://mkavana.gitlab.io/wordpress-eclecticswingfolk](https://mkavana.gitlab.io/wordpress-eclecticswingfolk)
